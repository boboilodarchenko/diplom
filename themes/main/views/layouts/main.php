<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.arcticmodal-0.2.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/site.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/holder.js"></script>
    <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.hotkeys.js"></script> -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.arcticmodal-0.2.min.js"></script>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/ico/favicon.png">
</head>

<body>

<div class="container-narrow">

    <div class="masthead">
        <?php $this->widget('zii.widgets.CMenu',array(
        'items'=>array(
            array('label'=>'Home', 'url'=>array('/site/index')),
            array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
            array('label'=>'Contact', 'url'=>array('/site/contact')),
            array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
        ),
        'htmlOptions' => array(
            'class'=>'nav nav-pills pull-right'
        ),
        )); ?>

	    <?php
	    if ( Yii::app()->user->isGuest && Yii::app()->getController()->getAction()->getId() !== 'login' ) {
		    Yii::app()->controller->renderPartial(
			    '//site/_loginInline',
			    array(
			         'model' => new LoginForm,
			    )
		    );
	    }
	    else {
	    ?>
            <h3 class="muted"><?php echo CHtml::link(Yii::app()->name,Yii::app()->homeUrl); ?></h3>
	    <?php }; ?>

    </div>

    <hr>

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        'links'=>$this->breadcrumbs,
        'tagName'=>'ul',
        'separator'=>'',
        'homeLink'=>false,
        'activeLinkTemplate'=>'<li><a href="{url}">{label}</a> <span class="divider">/</span></li>',
        'inactiveLinkTemplate'=>'<li class="active">{label}</li>',
        'htmlOptions'=>array('class'=>'breadcrumb'),
    )); ?><!-- breadcrumbs -->
    <?php endif?>

	<?php
	Yii::import('ext.EUserFlash');

	EUserFlash::setNoticeMessage('This is a setNoticeMessage message.');
	EUserFlash::setSuccessMessage('This is a success message.');
	EUserFlash::setErrorMessage('This is a error message.','login,activate');
	EUserFlash::setWarningMessage('This is a setWarningMessage message.');
	//EUserFlash::setAlertMessage('Hello world');
	//$url = $this->createUrl('userflash/id/1');
	//EUserFlash::setAjaxMessage($url);
	//this will render all flashes, independent of the context
	$this->widget( 'ext.EUserFlash',
	               array(
	                    'bootstrapLayout' => true,
	                    'initScript'=>"$('.userflash_success').fadeOut(10000);$('.userflash_notice').fadeOut(10000);"
	               )
	);

	//this is equivalent, but will return the count of rendered message if needed
	//see Special usage
	EUserFlash::renderFlashes($this);

	?>

    <?php echo $content; ?>

    <hr>

    <div class="footer">
        Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div>

</div> <!-- /container -->

</body>
</html>
