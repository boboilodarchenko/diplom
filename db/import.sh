#!/bin/sh

USER="root"
PASS="1"
DATABASE="corporate_portal"
path=`expr $0 : '^\(.*\)/import\.sh'`


if [$PASS == ""]; then
    mysql -u$USER $DATABASE < $path/schema.sql
else
    mysql -u$USER -p$PASS $DATABASE < $path/schema.sql
fi
