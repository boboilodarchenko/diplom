@ECHO OFF


set SERVER=localhost
set USER=root
set PASSWORD=1
set DATABASE=corporate_portal

IF "!PASSWORD!" == "" SET PASSWORD=-p %PASSWORD%

SET mysql=c:/xampp/xampp/mysql/bin/mysql
SET import=-h%SERVER% -u%USER% %PASSWORD% %DATABASE%

echo Import
call %mysql% %import% < schema.sql

pause
