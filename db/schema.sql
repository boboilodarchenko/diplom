SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `corporate_portal` ;
CREATE SCHEMA IF NOT EXISTS `corporate_portal` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
USE `corporate_portal` ;

-- -----------------------------------------------------
-- Table `corporate_portal`.`pages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`pages` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`pages` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `description` TEXT NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `captain_id` INT(11) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_pages_users1_idx` (`captain_id` ASC) ,
  CONSTRAINT `fk_pages_users1`
    FOREIGN KEY (`captain_id` )
    REFERENCES `corporate_portal`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`faculty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`faculty` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`faculty` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `number` INT(11) NULL ,
  `description` TEXT NULL ,
  `pages_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `pages_id`) ,
  INDEX `fk_faculty_pages1_idx` (`pages_id` ASC) ,
  CONSTRAINT `fk_faculty_pages1`
    FOREIGN KEY (`pages_id` )
    REFERENCES `corporate_portal`.`pages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`special`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`special` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`special` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `number` INT(11) NULL ,
  `faculty_id` INT NOT NULL ,
  `description` TEXT NULL ,
  `pages_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `faculty_id`, `pages_id`) ,
  INDEX `fk_special_faculty1_idx` (`faculty_id` ASC) ,
  INDEX `fk_special_pages1_idx` (`pages_id` ASC) ,
  CONSTRAINT `fk_special_faculty1`
    FOREIGN KEY (`faculty_id` )
    REFERENCES `corporate_portal`.`faculty` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_special_pages1`
    FOREIGN KEY (`pages_id` )
    REFERENCES `corporate_portal`.`pages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`groups` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`groups` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `number` INT(11) NULL ,
  `description` TEXT NULL ,
  `special_id` INT NOT NULL ,
  `pages_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `special_id`, `pages_id`) ,
  INDEX `fk_groups_special1_idx` (`special_id` ASC) ,
  INDEX `fk_groups_pages1_idx` (`pages_id` ASC) ,
  UNIQUE INDEX `pages_id_UNIQUE` (`pages_id` ASC) ,
  CONSTRAINT `fk_groups_special1`
    FOREIGN KEY (`special_id` )
    REFERENCES `corporate_portal`.`special` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_pages1`
    FOREIGN KEY (`pages_id` )
    REFERENCES `corporate_portal`.`pages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`users` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_name` VARCHAR(45) NULL ,
  `email` VARCHAR(255) NULL ,
  `first_name` VARCHAR(128) NULL ,
  `last_name` VARCHAR(128) NULL ,
  `sex` TINYINT(1) NULL ,
  `birthday` DATE NULL ,
  `language` VARCHAR(45) NULL ,
  `city` VARCHAR(45) NULL ,
  `country` VARCHAR(45) NULL ,
  `about` TEXT NULL ,
  `mobile_phone` VARCHAR(30) NULL ,
  `home_phone` VARCHAR(30) NULL ,
  `office_phone` VARCHAR(30) NULL ,
  `skype` VARCHAR(45) NULL ,
  `notifications` TINYINT(1) NULL ,
  `password` VARCHAR(45) NULL ,
  `role` TINYINT(1) NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `status` TINYINT(1) NULL ,
  `groups_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_users_groups1_idx` (`groups_id` ASC) ,
  CONSTRAINT `fk_users_groups1`
    FOREIGN KEY (`groups_id` )
    REFERENCES `corporate_portal`.`groups` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`message`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`message` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`message` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `text` VARCHAR(45) NULL ,
  `create_time` DATETIME NULL ,
  `update_time` DATETIME NULL ,
  `status` TINYINT(1) NULL ,
  `pages_id` INT NULL ,
  `users_id` INT NULL ,
  `to_user` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_message_pages1_idx` (`pages_id` ASC) ,
  INDEX `fk_message_users1_idx` (`users_id` ASC) ,
  CONSTRAINT `fk_message_pages1`
    FOREIGN KEY (`pages_id` )
    REFERENCES `corporate_portal`.`pages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_users1`
    FOREIGN KEY (`users_id` )
    REFERENCES `corporate_portal`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`files` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`files` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `file_name` VARCHAR(255) NULL ,
  `mime` VARCHAR(128) NULL ,
  `create_time` DATETIME NULL ,
  `message_id` INT NULL ,
  `pages_id` INT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_files_message1_idx` (`message_id` ASC) ,
  INDEX `fk_files_pages1_idx` (`pages_id` ASC) ,
  CONSTRAINT `fk_files_message1`
    FOREIGN KEY (`message_id` )
    REFERENCES `corporate_portal`.`message` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_pages1`
    FOREIGN KEY (`pages_id` )
    REFERENCES `corporate_portal`.`pages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`AuthItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`AuthItem` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`AuthItem` (
  `name` VARCHAR(64) NOT NULL ,
  `type` INT NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`AuthItemChild`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`AuthItemChild` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`AuthItemChild` (
  `parent` VARCHAR(64) NOT NULL ,
  `child` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`parent`, `child`) ,
  INDEX `fk_{EB6A9B37-64BD-4A72-A2A0-F72A54024CD5}` (`child` ASC) ,
  CONSTRAINT `fk_{F24CA340-C248-4554-B673-35A6AC286A1C}`
    FOREIGN KEY (`parent` )
    REFERENCES `corporate_portal`.`AuthItem` (`name` )
    ON DELETE cascade
    ON UPDATE cascade,
  CONSTRAINT `fk_{EB6A9B37-64BD-4A72-A2A0-F72A54024CD5}`
    FOREIGN KEY (`child` )
    REFERENCES `corporate_portal`.`AuthItem` (`name` )
    ON DELETE cascade
    ON UPDATE cascade)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corporate_portal`.`AuthAssignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `corporate_portal`.`AuthAssignment` ;

CREATE  TABLE IF NOT EXISTS `corporate_portal`.`AuthAssignment` (
  `itemname` VARCHAR(64) NOT NULL ,
  `userid` VARCHAR(64) NOT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`itemname`, `userid`) ,
  CONSTRAINT `fk_{8A9822FE-0315-4BB5-A0C2-6955380CF147}`
    FOREIGN KEY (`itemname` )
    REFERENCES `corporate_portal`.`AuthItem` (`name` )
    ON DELETE cascade
    ON UPDATE cascade)
ENGINE = InnoDB;

USE `corporate_portal` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `corporate_portal`.`pages`
-- -----------------------------------------------------
START TRANSACTION;
USE `corporate_portal`;
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, 'faculty', 'faculty', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, 'special', 'special', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, 'group', 'group', NULL, NULL, 0);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '1', '1', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '2', '2', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '3', '3', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '5', '5', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '6', '6', NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`pages` (`id`, `name`, `description`, `create_time`, `update_time`, `captain_id`) VALUES (NULL, '7', '7', NULL, NULL, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `corporate_portal`.`faculty`
-- -----------------------------------------------------
START TRANSACTION;
USE `corporate_portal`;
INSERT INTO `corporate_portal`.`faculty` (`id`, `name`, `number`, `description`, `pages_id`) VALUES (NULL, 'faculty', 1, 'faculty description', 1);
INSERT INTO `corporate_portal`.`faculty` (`id`, `name`, `number`, `description`, `pages_id`) VALUES (NULL, 'faculty2', 2, 'faculty2 description', 2);

COMMIT;

-- -----------------------------------------------------
-- Data for table `corporate_portal`.`special`
-- -----------------------------------------------------
START TRANSACTION;
USE `corporate_portal`;
INSERT INTO `corporate_portal`.`special` (`id`, `name`, `number`, `faculty_id`, `description`, `pages_id`) VALUES (NULL, 'special', 1, 1, 'special description', 3);
INSERT INTO `corporate_portal`.`special` (`id`, `name`, `number`, `faculty_id`, `description`, `pages_id`) VALUES (NULL, 'special2', 2, 1, 'special2 description', 4);
INSERT INTO `corporate_portal`.`special` (`id`, `name`, `number`, `faculty_id`, `description`, `pages_id`) VALUES (NULL, 'special3', 3, 2, 'special2 dddd', 5);
INSERT INTO `corporate_portal`.`special` (`id`, `name`, `number`, `faculty_id`, `description`, `pages_id`) VALUES (NULL, 'special3', 4, 2, 'specialspecial', 6);

COMMIT;

-- -----------------------------------------------------
-- Data for table `corporate_portal`.`groups`
-- -----------------------------------------------------
START TRANSACTION;
USE `corporate_portal`;
INSERT INTO `corporate_portal`.`groups` (`id`, `name`, `number`, `description`, `special_id`, `pages_id`) VALUES (NULL, 'groups', NULL, 'groupsgroups', 1, 7);
INSERT INTO `corporate_portal`.`groups` (`id`, `name`, `number`, `description`, `special_id`, `pages_id`) VALUES (NULL, 'groups2', NULL, 'groupsgroups', 2, 8);
INSERT INTO `corporate_portal`.`groups` (`id`, `name`, `number`, `description`, `special_id`, `pages_id`) VALUES (NULL, 'groups3', NULL, 'groupsgroups', 3, 9);

COMMIT;

-- -----------------------------------------------------
-- Data for table `corporate_portal`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `corporate_portal`;
INSERT INTO `corporate_portal`.`users` (`id`, `user_name`, `email`, `first_name`, `last_name`, `sex`, `birthday`, `language`, `city`, `country`, `about`, `mobile_phone`, `home_phone`, `office_phone`, `skype`, `notifications`, `password`, `role`, `create_time`, `update_time`, `status`, `groups_id`) VALUES (NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1$v63.Mp0.$uxCEyfcnMx3ZmLIdg7zdg0', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `corporate_portal`.`users` (`id`, `user_name`, `email`, `first_name`, `last_name`, `sex`, `birthday`, `language`, `city`, `country`, `about`, `mobile_phone`, `home_phone`, `office_phone`, `skype`, `notifications`, `password`, `role`, `create_time`, `update_time`, `status`, `groups_id`) VALUES (NULL, 'demo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$1$v63.Mp0.$uxCEyfcnMx3ZmLIdg7zdg0', NULL, NULL, NULL, NULL, NULL);

COMMIT;
