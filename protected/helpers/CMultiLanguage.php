<?php

/**
* Class MultiLange
*/
class CMultiLanguage
{

	/**
	 * Metod return string url list
	 * @return string Urls list language
	 */
	static function getListUrl()
	{
		foreach (Yii::app()->UrlManager->listLanguage() as $language => $languageUrl) {
			$urls .= '<ul>';
			if (Yii::app()->language==$language) {
				$urls .= '<li>'.$language.'</li>';
			} else {
				$urls .= '<li>'.CHtml::link($language,$languageUrl).'</li>';
			}
			$urls .= '</ul>';
		}
		return $urls;
	}

}