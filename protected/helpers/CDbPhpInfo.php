<?php

/**
* Class indo php and db
*/
class CDbPhpInfo
{
	static function infoAll()
	{
		$dbStats = Yii::app()->db->getStats();
		$memory = round(Yii::getLogger()->memoryUsage/1024/1024, 3);
		$time = round(Yii::getLogger()->executionTime, 3);

		$data .= '<br />Выполнено запросов: '.$dbStats[0].' (за '.round($dbStats[1], 5).' сек)'; 
		$data .= '<br />Использовано памяти: '.$memory.' МБ<br />';
		$data .= 'Время выполнения: '.$time.' с<br />';

		return $data;
	}

	static function infoSmall()
	{
		$dbStats = Yii::app()->db->getStats();
		$memory = round(Yii::getLogger()->memoryUsage/1024/1024, 3);
		$time = round(Yii::getLogger()->executionTime, 3);
		
		$data .= '(db:'.$dbStats[0].'-t:'.round($dbStats[1], 5).'s.)'; 
		$data .= '(m:'.$memory.'mb-';
		$data .= 't.e: '.$time.'s)';

		return $data;
	}
}