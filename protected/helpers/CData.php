<?php

/**
* 
*/
class CData
{


    /**
     * 
     */
    public static function date($date, $format = '')
    {
        if (empty($format )){
            $format = Yii::app()->params->dateFormat;
        }
        return Yii::app()->dateFormatter->format($format, $date);
    }

    /**
     * 
     */
    public static function datetime($date, $format = '', $inGmt = true )
    {
        if ( empty( $format ) ) {
            $format = Yii::app()->params->datetimeFormat;
        }

        $timeZote = new DateTimeZone( Yii::app()->params->timeZone );
        $gmt = new DateTimeZone('GMT');
        if( $inGmt ) {
            $time = new DateTime( $date, $gmt );
            $time->setTimezone( $timeZote );
        }
        else {
            $time = new DateTime( $date, $timeZote );
            $time->setTimezone( $gmt );
        }
        return $time->format($format);
    }

    /**
     * 
     */
    public static function dateTimestamp( $date )
    {
        $result = 0;
        if ( !empty( $date ) ) {
            $date   = explode( '-', $date );
            $result = mktime( 0, 0, 0, $date[1], $date[2], $date[0] );
        }

        return $result;
    }
}