<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'       => dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..',
	'name'           => 'Diplom portal',
	'charset'        => 'UTF-8',
	'sourceLanguage' => 'en_US',
	//'language'       => 'uk',
	'theme'          => 'main',
	//'defaultController'=>'Post',

	// preloading 'log' component
	'preload'        => array( 'log' , 'input' ),

	// autoloading model and component classes
	'import'         => array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'application.vendors.*',
	),

	'modules'        => array(
		// uncomment the following to enable the Gii tool
		'gii' => array(
			'class'     => 'system.gii.GiiModule',
			'password'  => '1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array( '*' ),
		),
	),

	// application components
	'components'     => array(
		'user'         => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'loginUrl'=>array('site/login'),

		),
		// uncomment the following to enable URLs in path-format
		'urlManager'   => array(
			'urlFormat'        => 'path',
			'showScriptName'   => false,
			'useStrictParsing' => true,
			'rules'            => array(
				'/'                                      => '/',
				'gii'                                    => 'gii',
				'gii/<controller:\w+>'                   => 'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'      => 'gii/<controller>/<action>',

				'<type:\w+>/<id:\d+>/page'      => 'page/view',

				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
				/* */
				/*'<a:\w+>/<controller:\w+>/<id:\d+>'              => '<a>/<controller>/view',
				'<a:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'        => '<a>/<controller>/<action>',
				'<a:\w+>/<controller:\w+>/<action:\w+>'              => '<a>/<controller>/<action>',*/

				'admin/structure'                             => 'admin/structure/index',
				'admin/structure/<_b:(delete|update)>/<id:\d+>/<model:\w+>' => 'admin/structure/<_b>',
				//'admin/structure/delete/<id:\d+>/<model:\w+>' => 'admin/structure/delete',
			),
		),
		// uncomment the following to use a MySQL database
		'db'           => array(
			'connectionString'      => 'mysql:host=localhost;dbname=corporate_portal',
			'emulatePrepare'        => true,
			'username'              => 'root',
			'password'              => '1',
			'charset'               => 'utf8',
			'enableProfiling'       => true,
			'enableParamLogging'    => true,
			'schemaCachingDuration' => 3600,
		),
		'authManager'  => array(
			'class'        => 'CDbAuthManager',
			'connectionID' => 'db',
		),
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				/*array(
					'class'		=>'CFileLogRoute',
					'levels'	=>'error, warning',
				),*/
				// uncomment the following to show log messages on web pages

				array(
					'class' => 'CWebLogRoute',
				),

			),
		),
		'request'      => array(
			//'class'=>'HttpRequest',
			'enableCsrfValidation'   => true,
			'csrfTokenName'          => 'TOKEN',
			'enableCookieValidation' => true,
		),
		'cache'        => array(
			//'class'=>'CDbCache',
			/*'servers'=>array(
				array('host'=>'server1', 'port'=>11211, 'weight'=>60),
				array('host'=>'server2', 'port'=>11211, 'weight'=>40),
			),*/
			//'class'=>'CFileCache',
			'class' => 'CDummyCache',
			//'cacheFileSuffix'=>'.txt',
		),
		'input'=>array(
			'class'         => 'CmsInput',
			'cleanPost'     => false,
			'cleanGet'      => false,
		),
		'clientScript'=>array(
			/*'scriptMap' => array(
				'jquery.js'=>false,  //disable default implementation of jquery
				'jquery.min.js'=>false,  //desable any others default implementation
				'core.css'=>false, //disable
				'styles.css'=>false,  //disable
				'pager.css'=>false,   //disable
				'default.css'=>false,  //disable
			),*/
			'packages'=>array(
				/*'jquery'=>array(                             // set the new jquery
					'baseUrl'=>'bootstrap/',
					'js'=>array('js/jquery-1.7.2.min.js','js/site.js'),
				),*/
				'bootstrap'=>array(                       //set others js libraries
					'baseUrl'=>'bootstrap/',
					'js'=>array('js/bootstrap.min.js'),
					'css'=>array(                        // and css
						'css/bootstrap.min.css',
						'css/custom.css',
						'css/bootstrap-responsive.min.css',
						//'css/bootstrap-responsive.css',
					),
					//'depends'=>array('jquery'),         // cause load jquery before load this.
				),
				'autosize'=>array(
					//'baseUrl'=>Yii::app()->request->baseUrl . '/js',
					'baseUrl'=>'js/',
					'js'=>array(
						'jquery.autosize-min.js',
					)
				)
			)
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'         => array(
		// this is used in contact page
		'adminEmail'     => 'boboilodarchenko@gmail.com',
		'datetimeFormat' => 'd M Y H:i', //1 May 2012 12:30
		'timeZone'       => 'Europe/Kiev',
	),
);
