<?php
/**
 * This abstract is the model class
 *
 * The followings are the available columns
 * @property string $create_time
 * @property string $update_time
 */

abstract class ActiveRecord extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if($this->isNewRecord)
        {
            $this->create_time=$this->update_time=gmdate( 'Y-m-d H:i:s' );
        } else {
            $this->update_time=gmdate( 'Y-m-d H:i:s' );
        };
        return parent::beforeSave();
    }
}