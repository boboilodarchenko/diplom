<?php

class StructureController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array( 'allow', // allow admin user to perform 'admin' and 'delete' actions
			       'actions' => array( 'index', 'update', 'delete' ),
			       'users'   => array( 'admin' ),
			),
			array( 'deny', // deny all users
			       'users' => array( '*' ),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
		$groupsPost  = Yii::app()->input->post( 'Groups' );
		$specialPost = Yii::app()->input->post( 'Special' );
		$facultyPost = Yii::app()->input->post( 'Faculty' );

		$groupsGet  = Yii::app()->input->get( 'Groups' );
		$specialGet = Yii::app()->input->get( 'Special' );
		$facultyGet = Yii::app()->input->get( 'Faculty' );

		$groups  = new Groups('search');
		$special = new Special('search');
		$faculty = new Faculty('search');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation(
			array(
			     $groups,
			     $special,
			     $faculty,
			)
		);

		$groups->unsetAttributes();  // clear any default values
		$groups->attributes=$groupsGet;

		$special->unsetAttributes();  // clear any default values
		$special->attributes=$specialGet;

		$faculty->unsetAttributes();  // clear any default values
		$faculty->attributes=$facultyGet;

		if(isset($groupsPost))
		{
			$groups->attributes=$groupsPost;
			if($groups->save())
				$this->refresh();
		}

		if(isset($specialPost))
		{
			$special->attributes=$specialPost;
			if($special->save())
				$this->refresh();
		}

		if(isset($facultyPost))
		{
			$faculty->attributes=$facultyPost;
			if($faculty->save())
				$this->refresh();
		}

			$this->render(
			'structure',
			array(
			     'groups'         => $groups,
			     'special'        => $special,
			     'faculty'        => $faculty,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer $id       the ID of the model to be loaded
	 * @param string $modelName the ID of the model to be loaded
	 *
	 */
	public function loadModel($id, $modelName)
	{
		$model=$modelName::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && ( $_POST['ajax']==='groups-form' || $_POST['ajax']==='special-form' || $_POST['ajax']==='faculty-form' ) )
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionUpdate($id, $model)
	{

		$this->renderText('success');
	}

	public function actionDelete( $id, $model )
	{
		$models = array( 'Groups', 'Special', 'Faculty' );
		if ( in_array( $model, $models ) ) {
			$model::model()->findByAttributes( array( 'id' => $id ) )->delete();
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if ( !isset( $_GET['ajax'] ) ) {
			$this->redirect( isset( $_POST['returnUrl'] ) ? $_POST['returnUrl'] : array( 'admin' ) );
		}
	}


}