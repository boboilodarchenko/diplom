<?php

class MessageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array( 'allow', // allow admin user to perform 'admin' and 'delete' actions
			       'actions' => array( 'get'),
			       'users'   => array( '@' ),
			),
			array( 'deny', // deny all users
			       'users' => array( '*' ),
			),
		);
	}

	/**
	 * Get message.
	 * Get message page and user.
	 */
	public function actionGet($type = null , $id  = null , $to_user  = null, $page  = null )
	{
		//$massage  = Yii::app()->input->post( 'Message' );
		$massage  = $_POST['Message'];
		if ( empty($massage) )
			throw new CHttpException(404,'The requested page does not exist.');

		$user_id = Yii::app()->user->getId();
		if ( !empty($type) && !empty($id) && empty($to_user) ) {
			$types = array( 'group', 'special', 'faculty' );
			if ( !in_array( $type, $types ) )
				throw new CHttpException(404,'The requested page does not exist.');

			switch ( $type ) {
				case 'group':
					$user=Users::model()->withGroup()->findByPk( $user_id );
					$pages_id = $user->groups->pages_id;
					if (!($user->groups->id == $id))
						throw new CHttpException(404,'The requested page does not exist.');

					//echo 'group';

					break;
				case 'special':
					$user=Users::model()->withSpecial()->findByPk( $user_id );
					$pages_id = $user->groups->special->pages_id;
					if (!($user->groups->special->id == $id))
						throw new CHttpException(404,'The requested page does not exist.');

					//echo 'special';

					break;
				case 'faculty':
					$user=Users::model()->withFaculty()->findByPk( $user_id );
					$pages_id = $user->groups->special->faculty->pages_id;
					if (!($user->groups->special->faculty->id == $id))
						throw new CHttpException(404,'The requested page does not exist.');
					//echo 'faculty';

					break;
			}

			$model = new Message;
			$model->setScenario('page');
			$model->attributes = $massage;
			$model->pages_id = $pages_id;
			//$model->type = 	$typeInt;
			$model->to_user = null;
			if ($model->save()){
				switch ( $type ) {
					case 'group':
						$model = Groups::model()->findByAttributes( array( 'id' => $id ) );
						break;
					case 'special':
						$model = Special::model()->findByAttributes( array( 'id' => $id ) );
						break;
					case 'faculty':
						$model = Faculty::model()->findByAttributes( array( 'id' => $id ) );
						break;
				}

				if( $model===null )
					throw new CHttpException(404,'The requested page does not exist.');

				$messages = $model->pages->messages(
					array(
					     'order'=>'id DESC',
					     'limit'=> 10,

					)
				);

				sort($messages);

		        if ( !empty( $messages ) ) {
			        foreach ( $messages as $message ) {
				        $this->renderPartial(
					        '_message',
					        array(
					             'message'=>$message,
					        )
				        );
			        }
		        }

			}
			else {
				print_r($model->getErrors());
			}

		}
		elseif( !empty($to_user) && empty($type) && empty($id) ) {
			$model = new Message;
			$model->setScenario('to_user');
			$model->attributes = $massage;
			$model->to_user = $to_user;
			$model->pages_id = null;
			//$model->type = 	null;
			if ($model->save()){

				$user_id = Yii::app()->user->getId();
				$messages = Yii::app()->db->createCommand()
					->select('t.* , u.user_name')
					->leftJoin('users u', 't.users_id=u.id')
					->from('message t')
					->where('( t.users_id=:user_id and t.to_user=:to_user ) or ( t.users_id=:to_user and t.to_user=:user_id )', array(':user_id'=>$user_id, ':to_user'=>$to_user))
					->limit(10)
					->order('t.id desc')
					->queryAll();

				sort($messages);

				if ( !empty( $messages ) ) {
					foreach ( $messages as $message ) {
						$this->renderPartial(
							'_messageIm',
							array(
							     'message'=>$message,
							)
						);
					}
				}

			}
		}
		else
			throw new CHttpException(404,'The requested page does not exist.');

		//$this->render('get');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}