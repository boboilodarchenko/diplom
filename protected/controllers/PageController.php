<?php

class PageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','im'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView( $id, $type )
	{

		if ( empty( $type ) || empty( $id ) )
				{
					throw new CHttpException( 404, 'The requested page does not exist.' );
				}

		$user_id = Yii::app()->user->getId();
		switch ( $type ) {
			case 'group':

				$user = Users::model()->withGroup()->findByPk( $user_id );
				if ( !( ( !empty( $user->groups->id ) ) && $user->groups->id == $id ) )
					throw new CHttpException( 404, 'The requested page does not exist.' );
				$model = Groups::model()->findByAttributes( array( 'id' => $id ) );

				break;
			case 'special':

				$user = Users::model()->withSpecial()->findByPk( $user_id );
				if ( !( ( !empty( $user->groups->special->id ) ) && $user->groups->special->id == $id ) )
					throw new CHttpException( 404, 'The requested page does not exist.' );
				$model = Special::model()->findByAttributes( array( 'id' => $id ) );

				break;
			case 'faculty':

				$user = Users::model()->withFaculty()->findByPk( $user_id );
				if ( !( ( !empty( $user->groups->special->faculty->id ) ) && $user->groups->special->faculty->id == $id ) )
					throw new CHttpException( 404, 'The requested page does not exist.' );
				$model = Faculty::model()->findByAttributes( array( 'id' => $id ) );

				break;
		}

		if ( empty( $model ) )
			throw new CHttpException( 404, 'The requested page does not exist.' );

		$page = $model->pages;

		$messages = $model->pages->messages(
			array(
			     'order' => 'id DESC',
			     'limit' => 10,
			)
		);

		sort( $messages );

		$massageForm = new Message;
		$this->render(
			'view',
			array(
			     'model'       => $model,
			     'type'        => $type,
			     'massageForm' => $massageForm,
			     'messages'    => $messages,
			     'page'        => $page,
			)
		);
	}

	/**
	 * Chat.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIm($to)
	{
		$user_id = Yii::app()->user->getId();
		$messages = Yii::app()->db->createCommand()
			->select('t.* , u.user_name')
			->leftJoin('users u', 't.users_id=u.id')
			->from('message t')
			->where('( t.users_id=:user_id and t.to_user=:to_user ) or ( t.users_id=:to_user and t.to_user=:user_id )', array(':user_id'=>$user_id, ':to_user'=>$to))
			->limit(10)
			->order('t.id desc')
			->queryAll();

		sort($messages);

		$massageForm = new Message;
		$this->renderPartial(
			'_im',
			array(
			     'massageForm' => $massageForm,
			     'messages'    => $messages,
			     'to_user' => $to,
			)
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	/*public function actionCreate()
	{
		$model=new Pages;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pages']))
		{
			$model->attributes=$_POST['Pages'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	/*public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pages']))
		{
			$model->attributes=$_POST['Pages'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}*/

	/**
	 * Lists all models.
	 */
	/*public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pages');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}*/

	/**
	 * Manages all models.
	 */
	/*public function actionAdmin()
	{
		$model=new Pages('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pages']))
			$model->attributes=$_GET['Pages'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	/*public function loadModel($id)
	{
		$model=Pages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}*/

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	/*protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}*/
}
