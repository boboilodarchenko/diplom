<?php

/**
 * This is the model class for table "files".
 *
 * The followings are the available columns in table 'files':
 * @property integer $id
 * @property string $name
 * @property string $file_name
 * @property string $mime
 * @property string $create_time
 * @property integer $message_id
 * @property integer $pages_id
 *
 * The followings are the available model relations:
 * @property Message $message
 * @property Pages $pages
 */
class Files extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Files the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message_id, pages_id', 'numerical', 'integerOnly'=>true),
			array('name, file_name', 'length', 'max'=>255),
			array('mime', 'length', 'max'=>128),
			array('create_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, file_name, mime, create_time, message_id, pages_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'message' => array(self::BELONGS_TO, 'Message', 'message_id'),
			'pages' => array(self::BELONGS_TO, 'Pages', 'pages_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'file_name' => 'File Name',
			'mime' => 'Mime',
			'create_time' => 'Create Time',
			'message_id' => 'Message',
			'pages_id' => 'Pages',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('mime',$this->mime,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('pages_id',$this->pages_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}