<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property integer $sex
 * @property string $birthday
 * @property string $language
 * @property string $city
 * @property string $country
 * @property string $about
 * @property string $mobile_phone
 * @property string $home_phone
 * @property string $office_phone
 * @property string $skype
 * @property integer $notifications
 * @property string $password
 * @property integer $role
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property integer $groups_id
 *
 * The followings are the available model relations:
 * @property Message[] $messages
 * @property Pages[] $pages
 * @property Groups $groups
 */
class Users extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('user_name, email, first_name, last_name', 'required'),
			array('sex, notifications, role, status, groups_id', 'numerical', 'integerOnly'=>true),
			array('user_name, language, city, country, skype, password', 'length', 'max'=>45),
			array('email', 'length', 'max'=>255),
			array('first_name, last_name', 'length', 'max'=>128),
			array('mobile_phone, home_phone, office_phone', 'length', 'max'=>30),
			array('birthday, about, create_time, update_time', 'safe'),

            array('mobile_phone, home_phone, office_phone', 'numerical', 'allowEmpty' => true, 'numberPattern' => '/^[\.\d() +-]+\d+$/'),
            array('email', 'email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_name, email, first_name, last_name, sex, birthday, language, city, country, about, mobile_phone, home_phone, office_phone, skype, notifications, password, role, create_time, update_time, status, groups_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Message', 'users_id'),
			'pages' => array(self::HAS_MANY, 'Pages', 'captain_id'),
			'groups' => array(self::BELONGS_TO, 'Groups', 'groups_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_name' => 'User Name',
			'email' => 'Email',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'sex' => 'Sex',
			'birthday' => 'Birthday',
			'language' => 'Language',
			'city' => 'City',
			'country' => 'Country',
			'about' => 'About',
			'mobile_phone' => 'Mobile Phone',
			'home_phone' => 'Home Phone',
			'office_phone' => 'Office Phone',
			'skype' => 'Skype',
			'notifications' => 'Notifications',
			'password' => 'Password',
			'role' => 'Role',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'status' => 'Status',
			'groups_id' => 'Groups',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('mobile_phone',$this->mobile_phone,true);
		$criteria->compare('home_phone',$this->home_phone,true);
		$criteria->compare('office_phone',$this->office_phone,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('notifications',$this->notifications);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('groups_id',$this->groups_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the declaration of named scopes.
	 *
	 * @see CActiveRecord::scopes
	 *
	 * @return array the scope definition. The array keys are scope names; the array
	 *      values are the corresponding scope definitions. Each scope definition is represented
	 *      as an array whose keys must be properties of {@link CDbCriteria}.
	 */
	public function scopes()
	{
		return array(
			'withGroup'          => array(
				'with' => array(
					'groups'=>array(
						'alias'=>'g'
					),

				)
			),
			'withSpecial'          => array(
				'with' => array(
					'groups'=>array(
						'alias'=>'g'
					),
					'groups.special'=>array(
						'alias'=>'gs'
					),
				)
			),
			'withFaculty'          => array(
				'with' => array(
					'groups'=>array(
						'alias'=>'g'
					),
					'groups.special'=>array(
						'alias'=>'gs'
					),
					'groups.special.faculty'=>array(
						'alias'=>'gsf'
					),
				)
			),
		);
	}

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            $this->password=crypt($this->password);
            return true;
        }
        else
            return false;
    }
}