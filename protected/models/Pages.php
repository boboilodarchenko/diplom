<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $create_time
 * @property string $update_time
 * @property integer $captain_id
 *
 * The followings are the available model relations:
 * @property Faculty[] $faculties
 * @property Files[] $files
 * @property Groups[] $groups
 * @property Message[] $messages
 * @property Users $captain
 * @property Special[] $specials
 */
class Pages extends ActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('captain_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('description, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, create_time, update_time, captain_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'faculties' => array(self::HAS_MANY, 'Faculty', 'pages_id'),
			'files' => array(self::HAS_MANY, 'Files', 'pages_id'),
			'groups' => array(self::HAS_MANY, 'Groups', 'pages_id'),
			'messages' => array(self::HAS_MANY, 'Message', 'pages_id'),
			'captain' => array(self::BELONGS_TO, 'Users', 'captain_id'),
			'specials' => array(self::HAS_MANY, 'Special', 'pages_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => 'ID',
			'name'        => 'Name',
			'description' => 'Description',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'captain_id'  => 'Captain',
		);
	}

	/**
	 * Returns the declaration of named scopes.
	 *
	 * @see CActiveRecord::scopes
	 *
	 * @return array the scope definition. The array keys are scope names; the array
	 *      values are the corresponding scope definitions. Each scope definition is represented
	 *      as an array whose keys must be properties of {@link CDbCriteria}.
	 */
	public function scopes()
	{
		return array(
			'withGroup'          => array(
				'with' => array(
					'groups'=>array(
						'alias'=>'g'
					),
					'groups.users'=>array(
						'alias'=>'gu'
					),

				)
			),
			'withSpecial'          => array(
				'with' => array(
					'specials'=>array(
						'alias'=>'s'
					),
					'specials.groups'=>array(
						'alias'=>'sg'
					),
					'specials.groups.users'=>array(
						'alias'=>'sgu'
					),
				)
			),
			'withFaculty'          => array(
				'with' => array(
					'faculties'=>array(
						'alias'=>'f'
					),
					'faculties.specials'=>array(
						'alias'=>'fs'
					),
					'faculties.specials.groups'=>array(
						'alias'=>'fsg'
					),
					'faculties.specials.groups.users'=>array(
						'alias'=>'fsgu'
					),
				)
			),
		);
	}

	public function defaultScope()
	{
		return array(
			'with'=>array(
				'messages',
				'messages.files',
				'files'=>array(
					'alias'=>'fi'
				),
			)
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.create_time',$this->create_time,true);
		$criteria->compare('t.update_time',$this->update_time,true);
		$criteria->compare('captain_id',$this->captain_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function createPages( $name, $description, $captain_id = null )
	{
		$pages              = new Pages;
		$pages->name        = $name;
		$pages->description = $description;
		$pages->captain_id  = $captain_id;
		$pages->save();

		return (int) $pages->id;
	}
}