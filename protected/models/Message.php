<?php

/**
 * This is the model class for table "message".
 *
 * The followings are the available columns in table 'message':
 * @property integer $id
 * @property string $text
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property integer $pages_id
 * @property integer $users_id
 * @property integer $to_user
 *
 * The followings are the available model relations:
 * @property Files[] $files
 * @property Pages $pages
 * @property Users $users
 */
class Message extends ActiveRecord
{

	/**
	 * Types
	 */
	/*const GROUP   = 1;
	const SPECIAL = 2;
	const FACULTY = 3;*/

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Message the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text', 'required'),
			array('status, pages_id, users_id, to_user', 'numerical', 'integerOnly'=>true),
			//array('text', 'length', 'max'=>45),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, text, create_time, update_time, status, pages_id, users_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'files' => array(self::HAS_MANY, 'Files', 'message_id'),
			'pages' => array(self::BELONGS_TO, 'Pages', 'pages_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'users_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'text' => 'Text',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'status' => 'Status',
			'pages_id' => 'Pages',
			'users_id' => 'Users',
			'to_user' => 'To user',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('pages_id',$this->pages_id);
		$criteria->compare('users_id',$this->users_id);
		$criteria->compare('to_user',$this->to_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave()
	{
		$this->users_id = Yii::app()->user->getId();
		return parent::beforeSave();
	}
}