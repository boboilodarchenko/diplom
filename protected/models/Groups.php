<?php

/**
 * This is the model class for table "groups".
 *
 * The followings are the available columns in table 'groups':
 * @property integer $id
 * @property string $name
 * @property integer $number
 * @property string $description
 * @property integer $special_id
 * @property integer $pages_id
 *
 * The followings are the available model relations:
 * @property Special $special
 * @property Pages $pages
 * @property Users[] $users
 */
class Groups extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Groups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'groups';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('special_id, name', 'required'),
			array('number, special_id, pages_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, number, description, special_id, pages_id, special', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'special' => array(self::BELONGS_TO, 'Special', 'special_id'),
			'pages' => array(self::BELONGS_TO, 'Pages', 'pages_id'),
			'users' => array(self::HAS_MANY, 'Users', 'groups_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'number' => 'Number',
			'description' => 'Description',
			'special_id' => 'Special',
			'pages_id' => 'Pages',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array( 'special' );

		$criteria->compare('t.id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('number',$this->number);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('special_id',$this->special_id);
		$criteria->compare('pages_id',$this->pages_id);

		$criteria->compare( 'special.name', $this->special, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'special'=>array(
						'asc'=>'special.name',
						'desc'=>'special.name DESC',
					),
					'*',
				),
			),
		));


	}

	/*public function defaultScope()
	{
		return array(
			'with'=>array(
				'users',
				'pages',
				'pages.messages',
				'pages.messages.files',
				'pages.files'=>array(
					'alias'=>'fi'
				),
			)
		);
	}*/

    public static function getAllTypes() {
		return CHtml::listData(Groups::model()->findAll(), 'id', 'name');
	}

	public function getModelName()
	{
		return __CLASS__;
	}

	public function beforeSave()
	{
		$this->pages_id  = Pages::createPages($this->name, $this->description);

		return parent::beforeSave();
	}
}