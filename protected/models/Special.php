<?php

/**
 * This is the model class for table "special".
 *
 * The followings are the available columns in table 'special':
 * @property integer $id
 * @property string $name
 * @property integer $number
 * @property integer $faculty_id
 * @property string $description
 * @property integer $pages_id
 *
 * The followings are the available model relations:
 * @property Groups[] $groups
 * @property Faculty $faculty
 * @property Pages $pages
 */
class Special extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Special the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'special';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('faculty_id, name', 'required'),
			array('number, faculty_id, pages_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, number, faculty_id, description, pages_id, faculty', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'groups' => array(self::HAS_MANY, 'Groups', 'special_id'),
			'faculty' => array(self::BELONGS_TO, 'Faculty', 'faculty_id'),
			'pages' => array(self::BELONGS_TO, 'Pages', 'pages_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'number' => 'Number',
			'faculty_id' => 'Faculty',
			'description' => 'Description',
			'pages_id' => 'Pages',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array( 'faculty' );

		$criteria->compare('t.id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('number',$this->number);
		$criteria->compare('faculty_id',$this->faculty_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('pages_id',$this->pages_id);

		$criteria->compare( 'faculty.name', $this->faculty, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'faculty'=>array(
						'asc'=>'faculty.name',
						'desc'=>'faculty.name DESC',
					),
					'*',
				),
			),
		));
	}

    public static function getAllTypes() {
		return CHtml::listData(Special::model()->findAll(), 'id', 'name');
	}

	public function getModelName()
	{
		return __CLASS__;
	}

	public function beforeSave()
	{
		$this->pages_id  = Pages::createPages($this->name, $this->description);

		return parent::beforeSave();
	}
}