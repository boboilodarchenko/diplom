<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<li class="span7">
    <div class="thumbnail">

        <h3><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?></h3>

        <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
        <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
        <?php echo CHtml::encode($data->user_name); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
        <?php echo CHtml::encode($data->email); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
        <?php echo CHtml::encode($data->first_name); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
        <?php echo CHtml::encode($data->last_name); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('sex')); ?>:</b>
        <?php echo CHtml::encode($data->sex); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('birthday')); ?>:</b>
        <?php echo CHtml::encode($data->birthday); ?>
        <br />

        <?php /*
        <b><?php echo CHtml::encode($data->getAttributeLabel('language')); ?>:</b>
        <?php echo CHtml::encode($data->language); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
        <?php echo CHtml::encode($data->city); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
        <?php echo CHtml::encode($data->country); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('about')); ?>:</b>
        <?php echo CHtml::encode($data->about); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('mobile_phone')); ?>:</b>
        <?php echo CHtml::encode($data->mobile_phone); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('home_phone')); ?>:</b>
        <?php echo CHtml::encode($data->home_phone); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('office_phone')); ?>:</b>
        <?php echo CHtml::encode($data->office_phone); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('skype')); ?>:</b>
        <?php echo CHtml::encode($data->skype); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('notifications')); ?>:</b>
        <?php echo CHtml::encode($data->notifications); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
        <?php echo CHtml::encode($data->password); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
        <?php echo CHtml::encode($data->role); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
        <?php echo CHtml::encode($data->create_time); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
        <?php echo CHtml::encode($data->update_time); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
        <?php echo CHtml::encode($data->status); ?>
        <br />

        <b><?php echo CHtml::encode($data->getAttributeLabel('groups_id')); ?>:</b>
        <?php echo CHtml::encode($data->groups_id); ?>
        <br />

        */ ?>

    </div>
</li>