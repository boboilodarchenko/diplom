<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>true,
    'htmlOptions' => array(
        'class'=>'form-horizontal',
    ),
    'clientOptions' => array(
	    'validateOnSubmit' => true,
	    'validateOnChange' => true,
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="control-group">
		<?php echo $form->labelEx($model,'user_name',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'user_name',array('size'=>45,'maxlength'=>45,'placeholder'=>$model->getAttributeLabel('user_name'))); ?>
            <?php echo $form->error($model,'user_name',array('class'=>'text-error')); ?>
        </div>
    </div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?>
        <div class="controls">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-envelope"></i></span>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255,'placeholder'=>$model->getAttributeLabel('email'))); ?>
            </div>
            <?php echo $form->error($model,'email',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'first_name',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>128,'placeholder'=>$model->getAttributeLabel('first_name'))); ?>
            <?php echo $form->error($model,'first_name',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'last_name',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>128,'placeholder'=>$model->getAttributeLabel('last_name'))); ?>
            <?php echo $form->error($model,'last_name',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'sex',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList($model,'sex',array(1=>'girl',2=>'boy'),array('empty'=>$model->getAttributeLabel('sex'))); ?>
            <?php echo $form->error($model,'sex',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'birthday',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'birthday'); ?>
            <?php echo $form->error($model,'birthday',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'language',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'language',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'language',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'city',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'city',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'city',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'country',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'country',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'country',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'about',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea($model,'about',array('rows'=>6, 'cols'=>50,'placeholder'=>$model->getAttributeLabel('about'))); ?>
            <?php echo $form->error($model,'about',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'mobile_phone',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'mobile_phone',array('size'=>30,'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('mobile_phone'))); ?>
            <?php echo $form->error($model,'mobile_phone',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'home_phone',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'home_phone',array('size'=>30,'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('home_phone'))); ?>
            <?php echo $form->error($model,'home_phone',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'office_phone',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'office_phone',array('size'=>30,'maxlength'=>30,'placeholder'=>$model->getAttributeLabel('office_phone'))); ?>
            <?php echo $form->error($model,'office_phone',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'skype',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'skype',array('size'=>45,'maxlength'=>45,'placeholder'=>$model->getAttributeLabel('skype'))); ?>
            <?php echo $form->error($model,'skype',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'notifications',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'notifications'); ?>
            <?php echo $form->error($model,'notifications',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->passwordField($model,'password',array('size'=>45,'maxlength'=>45,'placeholder'=>$model->getAttributeLabel('password'))); ?>
            <?php echo $form->error($model,'password',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'role',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'role'); ?>
            <?php echo $form->error($model,'role',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'status',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'status'); ?>
            <?php echo $form->error($model,'status',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'groups_id',array('class'=>'control-label')); ?>
        <div class="controls">
	        <?php echo $form->dropDownList($model,'groups_id', Groups::getAllTypes(),array('empty'=>$model->getAttributeLabel('groups_id'))); ?>
            <?php echo $form->error($model,'groups_id',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
        <div class="controls">
		    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
        </div>
	</div>

<?php $this->endWidget(); ?>

