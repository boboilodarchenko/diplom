<?php
$form = $this->beginWidget(
	'CActiveForm', array(
	                    'id'                     => 'login-form-inline',
	                    'action'               =>  array('site/login'),
	                    'enableClientValidation' => false,
	                    'clientOptions'          => array(
		                    'validateOnSubmit' => false,
	                    ),
	                    'htmlOptions'            => array(
		                    'class' => 'form-inline'
	                    ),
	               )
);
?>

		<?php echo $form->textField($model,'username', array('class'=>'input-small', 'placeholder'=>$model->getAttributeLabel('username') )); ?>
		<?php echo $form->error($model,'username',array('class'=>'text-error')); ?>


		<?php echo $form->passwordField($model,'password', array('class'=>'input-small', 'placeholder'=>$model->getAttributeLabel('password') )); ?>
		<?php echo $form->error($model,'password',array('class'=>'text-error')); ?>



        <label class="checkbox">
			<?php echo $form->checkBox($model,'rememberMe'); ?> <?php echo $model->getAttributeLabel('rememberMe'); ?>
        </label>
		<?php echo CHtml::submitButton('Sign in',array('class'=>'btn')); ?>


<?php $this->endWidget(); ?>