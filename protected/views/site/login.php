<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions' => array(
        'class'=>'form-horizontal'
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="control-group">
		<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username',array('class'=>'text-error')); ?>
        </div>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
        <div class="controls">
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password',array('class'=>'text-error')); ?>
        </div>
	</div>

    <div class="control-group">
        <div class="controls">
            <label class="checkbox">
                <?php echo $form->checkBox($model,'rememberMe'); ?> <?php echo $model->getAttributeLabel('rememberMe'); ?>
            </label>
            <?php echo CHtml::submitButton('Login',array('class'=>'btn')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

