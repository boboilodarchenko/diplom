<?php
/* @var $this StructureController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Structure',
);

?>

<h1> Manager structure </h1>

<div class="row-fluid">
    <div class="span4">
        <h2> _groups </h2>
	    <?php echo $this->renderPartial('_groups', array('model'=>$groups)); ?>
    </div>

    <div class="span4">
        <h2> _special </h2>
	    <?php echo $this->renderPartial('_special', array('model'=>$special)); ?>
    </div>

    <div class="span4">
        <h2> _faculty </h2>
	    <?php echo $this->renderPartial('_faculty', array('model'=>$faculty)); ?>
    </div>
</div>

<div class="row-fluid">
    <h3> _groups </h3>
	<?php
	$this->widget(
		'zii.widgets.grid.CGridView',
		array(
		     'id'            => 'groups-grid',
		     'itemsCssClass' => 'table table-bordered',
		     'dataProvider'  => $groups->search(),
		     'filter'        => $groups,
		     'columns'       => array(
			     'id',
			     'name',
			     array(            // display 'author.username' using an expression
				     'name'=>'special',
				     //'value'=>'$data->special->name',
				     //'url'=>'Yii::app()->createUrl("admin/structure/delete", array("id"=>$data->id,"model"=>$data->getModelName()))',
				     'type'=>'raw',
				     //'value'=>'Yii::app()->createUrl("page/view",array("id"=>$data->pages_id))',
				     'value'=>'CHtml::link($data->special->name, "?Special[id]=".$data->special->id."#special-grid")',
			     ),

			     array(
				     'class'=>'CLinkColumn',
				     'header'=>'pages_id',
				     //'labelExpression'=>'$data->pages_id',
				     'label'=>'pages_id',
				     'urlExpression'=>'Yii::app()->createUrl("page/view", array("id"=>$data->id, "type"=>"group"))',
			     ),
			     array(
				     'class' => 'CButtonColumn',
				     'template' => '{update} {delete}',
				     //'viewButtonUrl'=>'Yii::app()->createUrl(\'admin/artwork/\'. $data->id)',
				     //'updateButtonUrl'=>'Yii::app()->createUrl(\'admin/artwork/update/\'. $data->id)',
				     //'deleteButtonUrl'=>'Yii::app()->createUrl(\'admin/artwork/delete/\'. $data->id)',
				     'afterDelete'=>'function(link,success,data){ $.fn.yiiGridView.update("special-grid"); }',
				     'buttons'=>array
				     (
					     'update' => array
					     (
						     //'label'=>'Send an e-mail to this user',
						     //'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
						     'url'=>'Yii::app()->createUrl("admin/structure/update", array("id"=>$data->id,"model"=>$data->getModelName()))',
					     ),
					     'delete' => array
					     (
						     //'label'=>'[-]',
						     //'url'=>'Yii::app()->createUrl("admin/structure", array("id"=>$data->id))',
						     'url'=>'Yii::app()->createUrl("admin/structure/delete", array("id"=>$data->id,"model"=>$data->getModelName()))',
						     //'visible'=>'$data->score > 0',
						     //'click'=>'function(){alert("Going down!");}',
					     ),
				     ),
			     ),
		     ),
		)
	);
	?>
</div>

<div class="row-fluid">
    <h3> _special </h3>
	<?php
	$this->widget(
		'zii.widgets.grid.CGridView',
		array(
		     'id'            => 'special-grid',
		     'itemsCssClass' => 'table table-bordered',
		     'dataProvider'  => $special->search(),
		     'filter'        => $special,
		     'columns'       => array(
			     'id',
			     'name',
			     array(            // display 'author.username' using an expression
				     'name'=>'faculty',
				     //'value'=>'$data->faculty->name',
				     'type'=>'raw',
				     //'value'=>'Yii::app()->createUrl("page/view",array("id"=>$data->pages_id))',
				     'value'=>'CHtml::link($data->faculty->name, "?Faculty[id]=".$data->faculty->id."#faculty-grid")',
			     ),
			     array(
				     'class'=>'CLinkColumn',
				     'header'=>'pages_id',
				     //'labelExpression'=>'$data->pages_id',
				     'label'=>'pages_id',
				     'urlExpression'=>'Yii::app()->createUrl("page/view", array("id"=>$data->id, "type"=>"special"))',
			     ),
			     array(
				     'class' => 'CButtonColumn',
				     'template' => '{update} {delete}',
				     'afterDelete'=>'function(link,success,data){ if(success) $.fn.yiiGridView.update("faculty-grid"); }',
				     'buttons'=>array
				     (
					     'update' => array
					     (
						     'url'=>'Yii::app()->createUrl("admin/structure/update", array("id"=>$data->id,"model"=>$data->getModelName()))',
					     ),
					     'delete' => array
					     (
						     'url'=>'Yii::app()->createUrl("admin/structure/delete", array("id"=>$data->id,"model"=>$data->getModelName()))',
						     'visible'=>'count($data->groups) == 0',
					     ),
				     ),
			     ),
		     ),
		)
	);
	?>
</div>

<div class="row-fluid">
    <h3> _faculty </h3>
	<?php
	$this->widget(
		'zii.widgets.grid.CGridView',
		array(
		     'id'            => 'faculty-grid',
		     'itemsCssClass' => 'table table-bordered',
		     'dataProvider'  => $faculty->search(),
		     'filter'        => $faculty,
		     'columns'       => array(
			     'id',
			     'name',
			     array(
				     'class'=>'CLinkColumn',
				     'header'=>'pages_id',
				     //'labelExpression'=>'$data->pages_id',
				     'label'=>'pages_id',
				     'urlExpression'=>'Yii::app()->createUrl("page/view", array("id"=>$data->id, "type"=>"faculty"))',
			     ),
			     array(
				     'class' => 'CButtonColumn',
				     'template' => '{update} {delete}',
				     'buttons'=>array
				     (
					     'update' => array
					     (
						     'url'=>'Yii::app()->createUrl("admin/structure/update", array("id"=>$data->id,"model"=>$data->getModelName()))',
					     ),
					     'delete' => array
					     (
						     'url'=>'Yii::app()->createUrl("admin/structure/delete", array("id"=>$data->id,"model"=>$data->getModelName()))',
						     'visible'=>'count($data->specials) == 0',
					     ),
				     ),
			     ),
		     ),
		)
	);
	?>
</div>


