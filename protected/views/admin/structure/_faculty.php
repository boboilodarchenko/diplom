<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'faculty-form',
	'enableAjaxValidation'=>true,
    'htmlOptions' => array(
        'class'=>'form-horizontal',
    ),
    'clientOptions' => array(
	    'validateOnSubmit' => true,
	    'validateOnChange' => true,
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model, null,null,array('class'=>'alert')); ?>

	<div class="control-group">
			<?php echo $form->labelEx($model,'name',array()); ?>
            <?php echo $form->textField($model,'name',array('size'=>45,'maxlength'=>45)); ?>
            <?php echo $form->error($model,'name',array('class'=>'text-error')); ?>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'description',array()); ?>
            <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'placeholder'=>$model->getAttributeLabel('about'))); ?>
            <?php echo $form->error($model,'description',array('class'=>'text-error')); ?>
	</div>

	<div class="control-group">
        <div class="controls">
		    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn')); ?>
        </div>
	</div>

<?php $this->endWidget(); ?>

