<?php foreach ( $users as $user ): if ($user->id === Yii::app()->user->getId()) continue; ?>
<dl>
    <dt></dt>
    <dd>
        <img src="js/holder.js/50x50/#c0f2a7:#99c185" class="img-rounded">
		<?php echo $user->user_name; ?>
        <span class="m-dotted" onclick="EDU.im('<?php echo $this->createUrl( 'page/im', array('to'=>$user->id) ); ?>',this );">+</span>
    </dd>
</dl>
<?php endforeach;?>