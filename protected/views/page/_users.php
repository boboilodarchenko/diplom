<?php if ($type == 'group' && !empty( $model->users )):?>
	<?php
		$this->renderPartial(
			'_user',
			array(
			     'users' => $model->users,
			)
		);
		?>

<?php elseif ( $type == 'special' && !empty( $model->groups ) ):?>
	<div class="accordion" id="accordion2">
	<?php foreach ( $model->groups as $group ): if (empty($group->users)) continue;  ?>
		<div class="accordion-group">
	        <div class="accordion-heading">
	            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $group->id; ?>">
		            <?php echo $group->name; ?>
	            </a>
	        </div>
		<div id="collapse<?php echo $group->id; ?>" class="accordion-body collapse">
            <div class="accordion-inner">
			<?php
			$this->renderPartial(
				'_user',
				array(
				     'users' => $group->users,
				)
			);
			?>
            </div>
        </div>
       </div>
	<?php endforeach;?>
    </div>
<?php elseif ( $type == 'faculty' && !empty( $model->specials ) ): ?>
<div class="accordion" id="accordion2">
	<?php foreach ( $model->specials as $special ): if (empty($special->groups)) continue;  ?>
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapses<?php echo $special->id; ?>">
				<?php echo $special->name; ?>
            </a>
        </div>
        <div id="collapses<?php echo $special->id; ?>" class="accordion-body collapse">
            <div class="accordion-inner">
				<?php foreach ( $special->groups as $group ): if (empty($group->users)) continue; ?>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#collapses<?php echo $special->id; ?>" href="#collapseg<?php echo $group->id; ?>">
				            <?php echo $group->name; ?>
                        </a>
                    </div>
                    <div id="collapseg<?php echo $group->id; ?>" class="accordion-body collapse">
                        <div class="accordion-inner">
		            <?php
		            $this->renderPartial(
			            '_user',
			            array(
			                 'users' => $group->users,
			            )
		            );
		            ?>
                        </div>
                    </div>
                </div>
				<?php endforeach;?>
            </div>
        </div>
    </div>
	<?php endforeach;?>
</div>
<?php endif; ?>