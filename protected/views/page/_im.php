<?php
/* @var $this PageController */
/* @var $model Pages */


/*$this->menu=array(
	array('label'=>'List Pages', 'url'=>array('index')),
	array('label'=>'Create Pages', 'url'=>array('create')),
	array('label'=>'Update Pages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Pages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pages', 'url'=>array('admin')),
);*/
?>

<div>
    <div class="b-modal" id="exampleModal">
        <div class="b-modal_close arcticmodal-close">X</div>

<div class="">
    <div class="row-fluid show-grid">
        <div id="im" class="span10" style="height: 300px; overflow: auto; border: 1px solid;">
			<?php
			if ( !empty( $messages ) ) {
				foreach ( $messages as $message ) {
					$this->renderPartial(
						'//message/_messageIm',
						array(
						     'message'=>$message,
						)
					);
				}
			}
			?>
            <!--Body content-->
        </div>
    </div>
    <div class="row-fluid show-grid">
        <div class="span7 offset2">
            <!--Massages content-->
			<?php
			$form = $this->beginWidget(
				'CActiveForm',
				array(
				     'id'                   => 'massage-form',
				     'action'               =>  array('message/get', 'to_user'=>$to_user ),
				     /*'enableAjaxValidation' => true,*/
				     'htmlOptions'          => array(
					     'class' => 'form-horizontal',
					     //'onSubmit' => 'return true;',
				     ),
				     /*'clientOptions'        => array(
					 'validateOnSubmit' => true,
					 'validateOnChange' => true,
				 ),*/
				)
			);
			?>

            <fieldset>
                <div class="show-grid">
                    <img src="js/holder.js/50x50/#c0f2a7:#99c185" class="img-rounded">
					<?php
					echo $form->textArea(
						$massageForm,
						'text',
						array(
						     'rows'        => 2,
						     'cols'        => 60,
						     'placeholder' => $massageForm->getAttributeLabel( 'text' ),
						     'onkeyup'=> 'EDU.enableButtonMessages(this);',
						)
					);
					?>
					<?php
					echo CHtml::submitButton(
						'Create',
						array(
						     'class'   => 'btn btn-small btn-primary',
						     'onclick' => 'return EDU.sendMassageIm("' . $this->createUrl( 'user/view' ) . '","#sdasd", this);',
						     'disabled' => 'disabled',
						)
					);
					?>
                </div>
            </fieldset>



			<?php $this->endWidget(); ?>

        </div>
    </div>
</div>


    </div>
</div>
