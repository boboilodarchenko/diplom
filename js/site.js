var EDU = new Object();


EDU.im = function ( url, e )
{
	$.arcticmodal({
		type: 'ajax',
		url: url
		//data      : 'to='+to,
		/*afterLoading: function(data, el) {
			alert('afterLoading');
		},
		afterLoadingOnShow: function(data, el) {
			alert('afterLoadingOnShow');
		}*/
	});

};

EDU.enableButtonMessages = function ( e )
{
	var input = $( "input", e.form );
	if ( $( e ).val().length > 0 ) {
		$( "input", e.form ).removeAttr( "disabled" );
	}
	else {
		$( "input", e.form ).attr( "disabled", "disabled" );
	}

};

EDU.sendMassageIm = function ( url, selectors, e )
{
	var form = $( e.form );
	if ( $( "textarea", form ).val().length !== 0 ) {
		$.ajax( {
			type      : 'POST',
			url       : form.attr( 'action' ),
			data      : form.serialize(),
			beforeSend: function ()
			{
				console.log( "beforeSend" );
			},
			success   : function ( data, textStatus )
			{
				console.log( "success" );
				$( "textarea", form ).val('');
				$( "input", e.form ).attr( "disabled", "disabled" );
				$( "#im" ).html(data);
				$('#im').scrollTop(999, 800, {queue:true});
			},
			error     : function ( XMLHttpRequest, textStatus, errorThrown )
			{
				console.log( "error" );
			}
		} );
	}
	return false;
};

EDU.sendMassage = function ( url, selectors, e )
{
	var form = $( e.form );
	if ( $( "textarea", form ).val().length !== 0 ) {
		$.ajax( {
			type      : 'POST',
			url       : form.attr( 'action' ),
			data      : form.serialize(),
			beforeSend: function ()
			{
				console.log( "beforeSend" );
			},
			success   : function ( data, textStatus )
			{
				console.log( "success" );
				$( "textarea", form ).val('');
				$( "input", e.form ).attr( "disabled", "disabled" );
				$( ".span8" ).html(data);
				$('.span8').scrollTop(999, 800, {queue:true});
			},
			error     : function ( XMLHttpRequest, textStatus, errorThrown )
			{
				console.log( "error" );
			}
		} );
	}
	return false;
};

/*EDU.ajaxUpdate = function ( url, selectors, type )
{
	type = typeof type !== 'undefined' ? type : 'GET';
	$.ajax( {
		type      : type,
		url       : url,
		beforeSend: function ()
		{
			console.log( "beforeSend" );
		},
		success   : function ( data, textStatus )
		{
			console.log( "success" );
		},
		error     : function ( XMLHttpRequest, textStatus, errorThrown )
		{
			console.log( "error" );
		}
	} );
	return false;
};*/

$( function ()
{
	//setInterval(function() { alert(1) }, 2000);

	$( 'textarea' ).autosize();
	$('.span8').scrollTop(999, 800, {queue:true});
} );

$.fn.yiiactiveform.getInputContainer = function ( attribute, form )
{
	if ( attribute.inputContainer === undefined ) {
		return form.find( '#' + attribute.inputID ).closest( 'div.control-group' );
	}
	else {
		return form.find( attribute.inputContainer ).filter( ':has("#' + attribute.inputID + '")' );
	}
};

$(".alert").alert();